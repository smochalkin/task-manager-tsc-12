package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findById(String id) {
        for (Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    @Override
    public Task findByName(String name) {
        for (Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(int index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(String id) {
        Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(String name) {
        Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(Integer index) {
        Task task = findByIndex(index);
        tasks.remove(task);
        return task;
    }

    @Override
    public Task updateById(String id, String name, String desc) {
        Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(desc);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String desc) {
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(desc);
        return task;
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Task updateStatusById(String id, Status status) {
        Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateStatusByName(String name, Status status) {
        Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateStatusByIndex(int index, Status status) {
        Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
