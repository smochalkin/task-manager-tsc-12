package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(String id) {
        for (Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    @Override
    public Project findByName(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        Project project = findByIndex(index);
        projects.remove(project);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String desc) {
        Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String desc) {
        Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public int getCount() {
        return projects.size();
    }

    @Override
    public Project updateStatusById(String id, Status status) {
        Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateStatusByName(String name, Status status) {
        Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateStatusByIndex(int index, Status status) {
        Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}
