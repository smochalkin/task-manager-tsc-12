package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String desc);

    Project updateByIndex(Integer index, String name, String desc);

    int getCount();

    Project updateStatusById(String id, Status status);

    Project updateStatusByName(String name, Status status);

    Project updateStatusByIndex(int index, Status status);

}
