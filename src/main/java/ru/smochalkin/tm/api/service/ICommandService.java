package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
