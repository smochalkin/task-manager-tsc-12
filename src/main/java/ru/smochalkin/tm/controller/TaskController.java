package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.ITaskController;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index++ + ". " + task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        Task task = taskService.findByIndex(--index);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        showTask(task);
    }

    @Override
    public void showTask(Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void removeById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("Task not found.");
        }
    }

    @Override
    public void removeByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Task task = taskService.removeByName(name);
        if (task == null) {
            System.out.println("Task not found.");
        }
    }

    @Override
    public void removeByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        Task task = taskService.removeByIndex(--index);
        if (task == null) {
            System.out.println("Task not found.");
        }
    }

    @Override
    public void updateById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        Task updatedTask = taskService.updateById(id, name, desc);
        if (updatedTask == null)
            System.out.println("Invalid values.");
    }

    @Override
    public void updateByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        index--;
        Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        Task updatedTask = taskService.updateByIndex(index, name, desc);
        if (updatedTask == null)
            System.out.println("Invalid values.");
    }

    @Override
    public void startById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusByName(name, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        index--;
        Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusByName(name, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        index--;
        Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        taskService.updateStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void updateStatusById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid status.");
            return;
        }
        taskService.updateStatusById(id, status);
    }

    @Override
    public void updateStatusByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid status.");
            return;
        }
        taskService.updateStatusByName(name, status);
    }

    @Override
    public void updateStatusByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        if (index == null) {
            System.out.println("Index should be a number.");
            return;
        }
        index--;
        Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Task not found.");
            return;
        }
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid status.");
            return;
        }
        taskService.updateStatusByIndex(index, status);
    }

}
