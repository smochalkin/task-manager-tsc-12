package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String desc) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateById(id, name, desc);
    }

    @Override
    public Task updateByIndex(Integer index, String name, String desc) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Task updateStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.updateStatusById(id, status);
    }

    @Override
    public Task updateStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.updateStatusByName(name, status);
    }

    @Override
    public Task updateStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getCount()) return null;
        if (status == null) return null;
        return taskRepository.updateStatusByIndex(index, status);
    }

}
